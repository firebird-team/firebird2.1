Author: Damyan Ivanov <dmn@debian.org>
Purpose: Move Firebird-maintained CFLAGS in FB_CFLAGS so that CFLAGS can be set
         independently by the build.
Comments:
    dpkg-buildpackage invokes make with certain variables in the
    environment, most notably CFLAGS. When such variable is ised in the
    makefile, it is marked for export by make, thus passing it to
    subsequent make invokations. CFLAGS is added to in make.rules, which
    is included in each and every target, each time enlarging CFLAGS.
    .
    The problem is not visible unless CFLAGS is given to make, i.e.
    .
        make                # works OK
        CFLAGS=-g make      # very big CFLAGS
    .
    This patch changes all the Makefiles to mangle FB_CFLAGS, which is not
    exported and therefore does not accumulate changes. Both FB_CFLAGS and
    CFLAGS are used for compiler command line.

--- a/builds/posix/make.rules
+++ b/builds/posix/make.rules
@@ -32,18 +32,18 @@
 
 
 # Please don't use compiler/platform specific flags here - nmcc 02-Nov-2002
-CFLAGS:= $(CFLAGS) -I$(SRC_ROOT)/include/gen -I$(SRC_ROOT)/include -I$(SRC_ROOT)/vulcan $(CPPFLAGS) -DNAMESPACE=Vulcan
+FB_CFLAGS:= $(FB_CFLAGS) -I$(SRC_ROOT)/include/gen -I$(SRC_ROOT)/include -I$(SRC_ROOT)/vulcan $(CPPFLAGS) -DNAMESPACE=Vulcan
 ifeq ($(STD_ICU),false)
-  CFLAGS:= $(CFLAGS) -I$(ROOT)/extern/icu/source/common -I$(ROOT)/extern/icu/source/i18n
+  FB_CFLAGS:= $(FB_CFLAGS) -I$(ROOT)/extern/icu/source/common -I$(ROOT)/extern/icu/source/i18n
 endif
 
 ifeq ($(IsProdTypeBuild),Y)
-  CFLAGS:= $(CFLAGS) $(PROD_FLAGS) -DPROD_BUILD
+  FB_CFLAGS:= $(FB_CFLAGS) $(PROD_FLAGS) -DPROD_BUILD
 else
-  CFLAGS:= $(CFLAGS) $(DEV_FLAGS) -DDEV_BUILD
+  FB_CFLAGS:= $(FB_CFLAGS) $(DEV_FLAGS) -DDEV_BUILD
 endif
 
-CXXFLAGS:= $(CXXFLAGS) $(CFLAGS)
+FB_CXXFLAGS:= $(FB_CXXFLAGS) $(FB_CFLAGS)
 
 # Here we have definitions for using the preprocessor.
 
@@ -86,26 +86,26 @@ $(OBJ)/%.cpp: $(SRC_ROOT)/%.epp
 .SUFFIXES: .lo .o .cpp .c
 
 .c.o:
-	$(CC) -c $(CFLAGS) $(CXX_INCLUDE_DIRS) $(VERSION_FLAG) $<
+	$(CC) -c $(CFLAGS) $(FB_CFLAGS) $(CXX_INCLUDE_DIRS) $(VERSION_FLAG) $<
 
 .cpp.o:
-	$(CXX) -c $(CXXFLAGS) $(CXX_INCLUDE_DIRS) $(VERSION_FLAG) $<
+	$(CXX) -c $(CXXFLAGS) $(FB_CXXFLAGS) $(CXX_INCLUDE_DIRS) $(VERSION_FLAG) $<
 
 $(OBJ)/jrd/%.o: $(SRC_ROOT)/jrd/$(PLATFORM_PATH)/%.cpp
-	$(CXX) $(CXXFLAGS) -c $(firstword $<) -o $@
+	$(CXX) $(CXXFLAGS) $(FB_CXXFLAGS) -c $(firstword $<) -o $@
 
 ifneq ($(strip $(PLATFORM_FALLBACK)),)
 $(OBJ)/jrd/%.o: $(SRC_ROOT)/jrd/$(PLATFORM_FALLBACK)/%.cpp
-	$(CXX) $(CXXFLAGS) -c $(firstword $<) -o $@
+	$(CXX) $(CXXFLAGS) $(FB_CXXFLAGS) -c $(firstword $<) -o $@
 endif
 $(OBJ)/%.o: $(SRC_ROOT)/%.c 
-	$(CC) $(CFLAGS) -c $(firstword $<) -o $@
+	$(CC) $(CFLAGS) $(FB_CFLAGS) -c $(firstword $<) -o $@
 
 $(OBJ)/%.o: $(OBJ)/%.cpp 
-	$(CXX) $(CXXFLAGS) -c $(firstword $<) -o $@
+	$(CXX) $(CXXFLAGS) $(FB_CXXFLAGS) -c $(firstword $<) -o $@
 
 $(OBJ)/%.o: $(SRC_ROOT)/%.cpp 
-	$(CXX) $(CXXFLAGS) -c $(firstword $<) -o $@
+	$(CXX) $(CXXFLAGS) $(FB_CXXFLAGS) -c $(firstword $<) -o $@
 
 .SUFFIXES: .epp .e
 
--- a/builds/posix/Makefile.in.examples
+++ b/builds/posix/Makefile.in.examples
@@ -34,7 +34,7 @@ ObjModuleType=std
 # Add the install include directory to the search path since the
 # examples need to build using those headers values.
 
-CFLAGS := $(CFLAGS) -I$(ROOT)/gen/firebird/include
+FB_CFLAGS := $(FB_CFLAGS) -I$(ROOT)/gen/firebird/include
 
 
 include     $(ROOT)/gen/make.defaults
--- a/builds/posix/make.defaults
+++ b/builds/posix/make.defaults
@@ -79,7 +79,7 @@ RealFirebirdPath = @NEW_FIREBIRD_DIR@
 GLOB_OPTIONS=
 # Possible use - profiling.
 #GLOB_OPTIONS= -pg
-CFLAGS:= $(CFLAGS) $(GLOB_OPTIONS)
+FB_CFLAGS:= $(FB_CFLAGS) $(GLOB_OPTIONS)
 
 #____________________________________________________________________________
 
--- a/builds/posix/prefix.example
+++ b/builds/posix/prefix.example
@@ -8,7 +8,7 @@ if $(eq $(ModuleName), "intl")
 endif
 
 # Firebird needs no RTTI
-CXXFLAGS:= $(CXXFLAGS) -fno-rtti
+FB_CXXFLAGS:= $(FB_CXXFLAGS) -fno-rtti
 
 OS_ServerFiles=inet_server.cpp
 
--- a/builds/posix/prefix.linux_amd64
+++ b/builds/posix/prefix.linux_amd64
@@ -21,7 +21,7 @@
 COMMON_FLAGS=-ggdb -DFB_SEND_FLAGS=MSG_NOSIGNAL -DLINUX -DAMD64 -pipe -MMD -fPIC -fmessage-length=0
 OPTIMIZE_FLAGS=-O3 -fno-omit-frame-pointer 
 WARN_FLAGS=-Wall -Wno-switch -Wno-parentheses -Wno-unknown-pragmas -Wno-unused-variable
-CXXFLAGS:= $(CXXFLAGS) -fno-rtti
+FB_CXXFLAGS:= $(FB_CXXFLAGS) -fno-rtti
 
 PROD_FLAGS=-DNDEBUG $(COMMON_FLAGS) $(OPTIMIZE_FLAGS)
 DEV_FLAGS=$(COMMON_FLAGS) $(WARN_FLAGS)
--- a/builds/posix/prefix.linux_generic
+++ b/builds/posix/prefix.linux_generic
@@ -22,7 +22,7 @@ COMMON_FLAGS=-DLINUX -pipe -MMD -fPIC -D
 
 PROD_FLAGS=-ggdb -O3 -DNDEBUG $(COMMON_FLAGS)
 DEV_FLAGS=-ggdb -p -Wall -Wno-switch $(COMMON_FLAGS)
-CXXFLAGS:= $(CXXFLAGS) -fno-rtti
+FB_CXXFLAGS:= $(FB_CXXFLAGS) -fno-rtti
 
 OS_ServerFiles=inet_server.cpp
 
--- a/builds/posix/prefix.linux_sparc32
+++ b/builds/posix/prefix.linux_sparc32
@@ -21,7 +21,7 @@
 COMMON_FLAGS=-m32 -DLINUX -pipe -MMD -fPIC -Dsparc -DFB_SEND_FLAGS=MSG_NOSIGNAL
 PROD_FLAGS=-ggdb -mcpu=ultrasparc -mtune=ultrasparc -O3 -DNDEBUG $(COMMON_FLAGS)
 DEV_FLAGS=-ggdb -p -Wall -Wno-switch  $(COMMON_FLAGS)
-CXXFLAGS:= $(CXXFLAGS) -fno-rtti
+FB_CXXFLAGS:= $(FB_CXXFLAGS) -fno-rtti
 
 OS_ServerFiles=inet_server.cpp
 
--- a/builds/posix/Makefile.in.boot.gpre
+++ b/builds/posix/Makefile.in.boot.gpre
@@ -30,7 +30,7 @@
 #
 ROOT=..
 ObjModuleType=boot
-CFLAGS += -DBOOT_BUILD
+FB_CFLAGS += -DBOOT_BUILD
 
 include     $(ROOT)/gen/make.defaults
 include     $(ROOT)/gen/make.platform
--- a/builds/posix/Makefile.in.client.fbudf
+++ b/builds/posix/Makefile.in.client.fbudf
@@ -30,7 +30,7 @@
 #
 ROOT=..
 ObjModuleType=superclient
-CFLAGS+=-DSUPERCLIENT @PTHREAD_CFLAGS@
+FB_CFLAGS+=-DSUPERCLIENT @PTHREAD_CFLAGS@
 
 include     $(ROOT)/gen/make.defaults
 include     $(ROOT)/gen/make.platform
--- a/builds/posix/Makefile.in.client.gbak
+++ b/builds/posix/Makefile.in.client.gbak
@@ -31,7 +31,7 @@
 
 ROOT=..
 ObjModuleType=std
-CFLAGS+=-DSTD_UTIL
+FB_CFLAGS+=-DSTD_UTIL
 
 include     $(ROOT)/gen/make.defaults
 include     $(ROOT)/gen/make.platform
--- a/builds/posix/Makefile.in.client.gfix
+++ b/builds/posix/Makefile.in.client.gfix
@@ -30,7 +30,7 @@
 #
 ROOT=..
 ObjModuleType=std
-CFLAGS+=-DSTD_UTIL
+FB_CFLAGS+=-DSTD_UTIL
 
 include     $(ROOT)/gen/make.defaults
 include     $(ROOT)/gen/make.platform
--- a/builds/posix/Makefile.in.client.gpre
+++ b/builds/posix/Makefile.in.client.gpre
@@ -30,7 +30,7 @@
 #
 ROOT=..
 ObjModuleType=std
-CFLAGS+=-DSTD_UTIL
+FB_CFLAGS+=-DSTD_UTIL
 
 include     $(ROOT)/gen/make.defaults
 include     $(ROOT)/gen/make.platform
--- a/builds/posix/Makefile.in.client.gsec
+++ b/builds/posix/Makefile.in.client.gsec
@@ -22,7 +22,7 @@
 #
 ROOT=..
 ObjModuleType=superclient
-CFLAGS+=-DSUPERCLIENT -DSTD_UTIL @PTHREAD_CFLAGS@
+FB_CFLAGS+=-DSUPERCLIENT -DSTD_UTIL @PTHREAD_CFLAGS@
 
 include     $(ROOT)/gen/make.defaults
 include     $(ROOT)/gen/make.platform
--- a/builds/posix/Makefile.in.client.util
+++ b/builds/posix/Makefile.in.client.util
@@ -30,7 +30,7 @@
 #
 ROOT=..
 ObjModuleType=std
-CFLAGS+=-DSTD_UTIL
+FB_CFLAGS+=-DSTD_UTIL
 
 include     $(ROOT)/gen/make.defaults
 include     $(ROOT)/gen/make.platform
--- a/builds/posix/Makefile.in.embed.fbudf
+++ b/builds/posix/Makefile.in.embed.fbudf
@@ -30,7 +30,7 @@
 #
 ROOT=..
 ObjModuleType=superclient
-CFLAGS+=-DSUPERCLIENT @PTHREAD_CFLAGS@
+FB_CFLAGS+=-DSUPERCLIENT @PTHREAD_CFLAGS@
 
 include     $(ROOT)/gen/make.defaults
 include     $(ROOT)/gen/make.platform
--- a/builds/posix/Makefile.in.embed.gbak
+++ b/builds/posix/Makefile.in.embed.gbak
@@ -32,7 +32,7 @@ ROOT=..
 SERVICES_THREADED=@service_thread_CS@
 ifeq ($(SERVICES_THREADED),true)
     ObjModuleType=superclient
-    CFLAGS+=-DSTD_UTIL -DSUPERCLIENT
+    FB_CFLAGS+=-DSTD_UTIL -DSUPERCLIENT
 else
     ObjModuleType=std
 endif
--- a/builds/posix/Makefile.in.embed.gfix
+++ b/builds/posix/Makefile.in.embed.gfix
@@ -32,7 +32,7 @@ ROOT=..
 SERVICES_THREADED=@service_thread_CS@
 ifeq ($(SERVICES_THREADED),true)
     ObjModuleType=superclient
-    CFLAGS+=-DSTD_UTIL -DSUPERCLIENT
+    FB_CFLAGS+=-DSTD_UTIL -DSUPERCLIENT
 else
     ObjModuleType=std
 endif
--- a/builds/posix/Makefile.in.embed.lockmgr
+++ b/builds/posix/Makefile.in.embed.lockmgr
@@ -31,7 +31,7 @@
 #
 ROOT=..
 ObjModuleType=boot
-CFLAGS += -DBOOT_BUILD
+FB_CFLAGS += -DBOOT_BUILD
 
 include     $(ROOT)/gen/make.defaults
 include     $(ROOT)/gen/make.platform
--- a/builds/posix/Makefile.in.embed.util
+++ b/builds/posix/Makefile.in.embed.util
@@ -32,7 +32,7 @@ ROOT=..
 SERVICES_THREADED=@service_thread_CS@
 ifeq ($(SERVICES_THREADED),true)
     ObjModuleType=superclient
-    CFLAGS+=-DSTD_UTIL -DSUPERCLIENT
+    FB_CFLAGS+=-DSTD_UTIL -DSUPERCLIENT
 else
     ObjModuleType=std
 endif
--- a/builds/posix/Makefile.in.extlib
+++ b/builds/posix/Makefile.in.extlib
@@ -30,7 +30,7 @@
 #
 ROOT=..
 ObjModuleType=std
-CFLAGS+=@PTHREAD_CFLAGS@
+FB_CFLAGS+=@PTHREAD_CFLAGS@
 
 include     $(ROOT)/gen/make.defaults
 include     $(ROOT)/gen/make.platform
--- a/builds/posix/Makefile.in.fbserver
+++ b/builds/posix/Makefile.in.fbserver
@@ -31,7 +31,7 @@
 #
 ROOT=..
 ObjModuleType=superserver
-CFLAGS+=-DSUPERSERVER @PTHREAD_CFLAGS@
+FB_CFLAGS+=-DSUPERSERVER @PTHREAD_CFLAGS@
 ArchType=super
 
 include     $(ROOT)/gen/make.defaults
--- a/builds/posix/Makefile.in.intl
+++ b/builds/posix/Makefile.in.intl
@@ -30,7 +30,7 @@
 #
 ROOT=..
 ObjModuleType=superclient
-CFLAGS+=-DSUPERCLIENT @PTHREAD_CFLAGS@
+FB_CFLAGS+=-DSUPERCLIENT @PTHREAD_CFLAGS@
 
 include     $(ROOT)/gen/make.defaults
 include     $(ROOT)/gen/make.platform
--- a/builds/posix/Makefile.in.libfbclient
+++ b/builds/posix/Makefile.in.libfbclient
@@ -30,7 +30,7 @@
 #
 ROOT=..
 ObjModuleType=superclient
-CFLAGS+=-DSUPERCLIENT @PTHREAD_CFLAGS@
+FB_CFLAGS+=-DSUPERCLIENT @PTHREAD_CFLAGS@
 
 include     $(ROOT)/gen/make.defaults
 include     $(ROOT)/gen/make.platform
--- a/builds/posix/Makefile.in.libfbstatic
+++ b/builds/posix/Makefile.in.libfbstatic
@@ -30,7 +30,7 @@
 #
 ROOT=..
 ObjModuleType=boot
-CFLAGS+=-DBOOT_BUILD
+FB_CFLAGS+=-DBOOT_BUILD
 
 include     $(ROOT)/gen/make.defaults
 include     $(ROOT)/gen/make.platform
--- a/builds/posix/Makefile.in.static.gbak
+++ b/builds/posix/Makefile.in.static.gbak
@@ -29,7 +29,7 @@
 #
 ROOT=..
 ObjModuleType=boot
-CFLAGS += -DBOOT_BUILD
+FB_CFLAGS += -DBOOT_BUILD
 
 include     $(ROOT)/gen/make.defaults
 include     $(ROOT)/gen/make.platform
--- a/builds/posix/Makefile.in.static.gpre
+++ b/builds/posix/Makefile.in.static.gpre
@@ -31,7 +31,7 @@
 #
 ROOT=..
 ObjModuleType=boot
-CFLAGS += -DBOOT_BUILD
+FB_CFLAGS += -DBOOT_BUILD
 
 include     $(ROOT)/gen/make.defaults
 include     $(ROOT)/gen/make.platform
--- a/builds/posix/Makefile.in.static.isql
+++ b/builds/posix/Makefile.in.static.isql
@@ -30,7 +30,7 @@
 #
 ROOT=..
 ObjModuleType=boot
-CFLAGS+= -DBOOT_BUILD
+FB_CFLAGS+= -DBOOT_BUILD
 
 include     $(ROOT)/gen/make.defaults
 include     $(ROOT)/gen/make.platform
