#!/bin/sh

set -eu

get_ver()
{
	awk "/FB_$1/ { v=\$3; gsub(\"\\\"\", \"\", v); print v}" < src/jrd/build_no.h
}

FB_MAJOR=$( get_ver 'MAJOR_VER' )
FB_MINOR=$( get_ver 'MINOR_VER' )
FB_REV=$( get_ver 'REV_NO' )

FB_VERSION="${FB_MAJOR}.${FB_MINOR}.${FB_REV}"

FB_VER="${FB_MAJOR}.${FB_MINOR}"
FB2="firebird$FB_VER"
FB2_no_dots=`echo $FB2 | sed -e 's/\.//g'`
FB2DIR="firebird/$FB_VER"
ULFB2="usr/lib/$FB2DIR"
USFB2="usr/share/$FB2DIR"
VAR="var/lib/$FB2DIR"
EMBED_SO_VER="${FB_MAJOR}.${FB_MINOR}"
CLIENT_SO_VER=${FB_MAJOR}


copy ()
{
    type=$1
    dest=$2
    shift
    shift

    case "$type" in
        e*) mode="755" ;;
        f*) mode="644" ;;
        *) echo "Error: Wrong params for copy!"; exit 1;;
    esac

    install -m $mode "$@" "$dest"
}

# Helper function used both in -super and -classic
copy_utils()
{
    for s in gbak gdef gfix gpre qli gsec gstat isql nbackup;
    do
        target=$s
        if [ $target = gstat ];
        then
            target=fbstat
        elif [ $target = isql ];
        then
            target=isql-fb
        fi

        copy e $D/$ULFB2/bin/$target.real $S/bin/$s
        ln -s /$ULFB2/bin/wrapper $D/usr/bin/$target
    done
}

COMMON_DOC=/usr/share/doc/$FB2-common-doc

doc_symlink() {
    local doc_root
    doc_root=debian/$P/usr/share/doc
    [ -d $doc_root ] || mkdir -p $doc_root
    ln -s $FB2-common-doc $doc_root/$P
}

#-super
make_super () {
    P="$FB2-super"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-super

    mkdir -p $D/usr/bin $D/$ULFB2/bin $D/$ULFB2/UDF

    for f in fbserver fbguard fb_lock_print fbmgr.bin; do
        install -m 0755 $S/bin/$f $D/$ULFB2/bin/$f.real
        ln -s wrapper $D/$ULFB2/bin/$f
    done

    copy e $D/$ULFB2/UDF $S/UDF/fbudf.so $S/UDF/ib_udf.so

    copy_utils

    doc_symlink
}

#-classic
make_classic () {
    P="$FB2-classic"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-classic

    mkdir -p $D/$ULFB2/bin      \
            $D/$ULFB2/UDF       \
            $D/usr/bin          \
            $D/etc/xinetd.d

    for f in fb_inet_server fb_lock_mgr fb_lock_print gds_drop; do
        install -m 0755 $S/bin/$f $D/$ULFB2/bin/$f.real
        ln -s wrapper $D/$ULFB2/bin/$f
    done

    copy_utils

    install -m 0644 debian/$FB2-classic.xinetd \
    		    $D/etc/xinetd.d/$FB2_no_dots

    copy e $D/$ULFB2/UDF $S/UDF/fbudf.so $S/UDF/ib_udf.so

    doc_symlink
}

#libfbclient
make_libfbclient () {
    P="libfbclient$CLIENT_SO_VER"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-super

    mkdir -p $D/usr/lib

    copy e $D/usr/lib $S/lib/libfbclient.so.$FB_VERSION
    ln -s libfbclient.so.$FB_VERSION $D/usr/lib/libfbclient.so.$CLIENT_SO_VER

    doc_symlink
}

#libfbembed
make_libfbembed () {
    P="libfbembed$EMBED_SO_VER"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-classic

    mkdir -p $D/usr/lib

    copy e $D/usr/lib $S/lib/libfbembed.so.$FB_VERSION
    ln -s libfbembed.so.$FB_VERSION $D/usr/lib/libfbembed.so.$EMBED_SO_VER

    doc_symlink
}


#-server-common
make_server_common () {
    P="$FB2-server-common"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-super

    mkdir -p $D/etc/$FB2DIR \
        $D/etc/logrotate.d \
        $D/etc/$FB2DIR \
        $D/$ULFB2 \
        $D/$ULFB2/UDF \
        $D/$ULFB2/bin \
        $D/$VAR \
        $D/$VAR/system \
        $D/$VAR/tmp \
        $D/$VAR/data \
        $D/$VAR/backup \
        $D/$COMMON_DOC/examples

    copy f $D/etc/$FB2DIR \
        $S/install/misc/aliases.conf

    # fix aliases.conf: employee.fdb should point to a database
    # in /$VAR/data where all databases live
    sed -i -e "s,/$ULFB2/examples/empbuild,/$VAR/data," \
        $D/etc/$FB2DIR/aliases.conf

    touch $D/$VAR/backup/no_empty
    touch $D/$VAR/data/no_empty

    copy f $D/$ULFB2/UDF \
        src/extlib/fbudf/fbudf.sql \
        src/extlib/ib_udf.sql \
        src/extlib/ib_udf2.sql

    # databases
    cp $S/security2.fdb \
    $D/$VAR/system/default-security2.fdb

    copy f $D/$VAR/system $S/help/help.fdb

    # manpages
    for u in fbstat gbak gdef gsec isql-fb gfix gpre qli nbackup ;
    do
        dh_installman -p $P debian/$u.1
    done

    copy f $D/$COMMON_DOC/examples debian/reindex-db

    doc_symlink
}

#-common
make_common () {
    P="$FB2-common"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-super

    mkdir -p \
        $D/etc/$FB2DIR \
        $D/$ULFB2/intl $D/$ULFB2/bin \
        $D/$USFB2 \
        $D/usr/share/$P

    # config
    copy f $D/etc/$FB2DIR $S/install/misc/firebird.conf

    install -m 0644 -o root -g root \
        debian/functions.sh \
        $D/usr/share/$P/

    for m in $S/*.msg;
    do
        copy f $D/$USFB2 $m
    done

    copy f $D/etc/$FB2DIR/fbintl.conf $S/misc/fbintl.conf

    install -m 0064 $S/intl/libfbintl.so $D/$ULFB2/intl/fbintl.so

    copy e $D/$ULFB2/bin debian/wrapper

    doc_symlink
}

#-dev
make_dev () {
    P="$FB2-dev"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-super

    mkdir -p $D/usr/include \
             $D/usr/lib/$DEB_HOST_MULTIARCH

    copy f $D/usr/include $S/include/*.h

    ln -s libfbclient.so.$CLIENT_SO_VER $D/usr/lib/$DEB_HOST_MULTIARCH/libfbclient.so
    ln -s libfbembed.so.$EMBED_SO_VER $D/usr/lib/$DEB_HOST_MULTIARCH/libfbembed.so

    doc_symlink
}


#-examples
make_examples () {
    P="$FB2-examples"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird-super

    mkdir -p $D/$COMMON_DOC
    cp -r $S/examples $D/$COMMON_DOC

    install -m 0644 \
        debian/$P.README.Debian \
        $D/$COMMON_DOC/examples/README.Debian

    doc_symlink
}

#-doc
make_doc () {
    P="$FB2-doc"
    echo "Creating $P content"
    D=debian/$P/$COMMON_DOC/doc
    S=doc

    mkdir -p $D

    cp -r $S/* $D/
    rm -r $D/license

    doc_symlink
}

#-common-doc
make_common_doc() {
    P="$FB2-common-doc"
    echo "Creating $P content"
    mkdir -p debian/$P/$COMMON_DOC
}

umask 022
make_super
make_classic
#make_libfbclient
make_libfbembed
make_common
make_server_common
#make_dev
#P=firebird${FB_VER}-dev doc_symlink
make_examples
make_doc
make_common_doc
echo "Packages ready."
exit 0
