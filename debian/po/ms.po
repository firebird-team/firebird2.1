# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: firebird2.1@packages.debian.org\n"
"POT-Creation-Date: 2008-05-23 13:35+0300\n"
"PO-Revision-Date: 2006-12-11 08:44+0800\n"
"Last-Translator: Nicholas Ng <nbliang@gmail.com>\n"
"Language-Team: Translation Project Team, Kuching Open Source Community "
"<translation@kuchingosc.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Malay\n"
"X-Poedit-Country: MALAYSIA\n"

#. Type: boolean
#. Description
#: ../server-templates.master:1001
msgid "Enable Firebird server?"
msgstr ""

#. Type: boolean
#. Description
#: ../server-templates.master:1001
msgid "Accept if you want Firebird server to start automatically."
msgstr ""

#. Type: boolean
#. Description
#: ../server-templates.master:1001
msgid ""
"If you only need the Firebird client and there are no databases that will be "
"served by this host, decline."
msgstr ""

#. Type: password
#. Description
#. Type: password
#. Description
#: ../server-templates.master:2001 ../server-templates.master:3001
msgid "Password for SYSDBA:"
msgstr "Kata laluan untuk SYSDBA:"

#. Type: password
#. Description
#. Type: password
#. Description
#: ../server-templates.master:2001 ../server-templates.master:3001
msgid ""
"Firebird has a special user named SYSDBA, which is the user that has access "
"to all databases. SYSDBA can also create new databases and users. Because of "
"this, it is necessary to secure SYSDBA with a password."
msgstr ""
"Firebird mempunyai pengguna khas bernama SYSDBA, yang mana pengguna ini "
"mempunyai akses ke semua pangkalan data. SYSDBA juga boleh mencipta "
"pangkalan data dan pengguna baru. Oleh itu, adalah penting untuk lindungi "
"dengan kata laluan."

#. Type: password
#. Description
#. Type: password
#. Description
#: ../server-templates.master:2001 ../server-templates.master:3001
msgid ""
"The password is stored in /etc/firebird/${FB_VER}/SYSDBA.password (readable "
"only by root). You may modify it there (don't forget to update the security "
"database too, using the gsec utility), or you may use dpkg-reconfigure to "
"update both."
msgstr ""
"Kata laluan disimpan di dalam /etc/firebird/${FB_VER}/SYSDBA.password (hanya "
"boleh dibaca oleh root). Anda boleh mengubahnya disitu (jangan lupa untuk "
"kemaskini pangkalan data keselamatan dengan menggunakan utiliti gsec), atau "
"anda boleh menggunakan dpkg-reconfigure untuk kemaskinikan kedua-duanya."

#. Type: password
#. Description
#: ../server-templates.master:2001
msgid ""
"If you don't enter a password, a random one will be used (and stored in "
"SYSDBA.password)."
msgstr ""
"Jika anda tidak masukkan kata laluan, kata laluan rawak akan digunakan (dan "
"disimpan di dalam SYSDBA.password)."

#. Type: password
#. Description
#: ../server-templates.master:3001
msgid "To keep your existing password, leave this blank."
msgstr "Untuk kekalkan kata laluan yang sedia ada, biarkan ini kosong."

#. Type: boolean
#. Description
#: ../server-templates.master:5001
msgid "Delete password database?"
msgstr ""

#. Type: boolean
#. Description
#: ../server-templates.master:5001
msgid ""
"The last package that uses password database at /var/lib/firebird/${FB_VER}/"
"system/security.fdb is being purged."
msgstr ""

#. Type: boolean
#. Description
#: ../server-templates.master:5001
msgid ""
"Leaving security database may present security risk. It is a good idea to "
"remove it if you don't plan re-installing firebird${FB_VER}."
msgstr ""

#. Type: boolean
#. Description
#: ../server-templates.master:5001
msgid ""
"The same stands for /etc/firebird/${FB_VER}/SYSDBA.password, where the "
"password for SYSDBA is kept."
msgstr ""

#. Type: boolean
#. Description
#: ../server-templates.master:6001
msgid "Delete databases from /var/lib/firebird/${FB_VER}/data?"
msgstr ""

#. Type: boolean
#. Description
#: ../server-templates.master:6001
msgid ""
"You may want to delete all databases from firebird standard database "
"directory, /var/lib/firebird/${FB_VER}/data. If you choose this option, all "
"files ending with \".fdb\" and \".fbk\" from the above directory and its "
"subdirectories will be removed."
msgstr ""

#. Type: boolean
#. Description
#: ../server-templates.master:6001
msgid ""
"Note that any databases outside of /var/lib/firebird/${FB_VER}/data will not "
"be affected."
msgstr ""

#. Type: error
#. Description
#: ../server-templates.master:7001
msgid "firebird${FB_VER}-${FB_FLAVOUR} server is in use"
msgstr ""

#. Type: error
#. Description
#: ../server-templates.master:7001
msgid ""
" To ensure data integrity, package removal/upgrade is aborted. Please stop "
"all local and remote clients before removing or upgrading firebird${FB_VER}-"
"${FB_FLAVOUR}"
msgstr ""

#. Type: title
#. Description
#: ../server-templates.master:8001
#, fuzzy
msgid "Password for firebird ${FB_VER}"
msgstr "Kata laluan untuk SYSDBA:"
