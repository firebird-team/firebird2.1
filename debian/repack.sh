#!/bin/sh
#
# Repack upstream source converting from bz2 to gz and
# removing some debian-supplied libraries and generated
# files in the process
#
# To be called via debian/watch (uscan or uscan --force)
# or
#  sh debian/repack.sh --upstream-version VER FILE

set -e
set -u

usage() {
    cat <<EOF >& 2
Usage: $0 --upstream-version VER FILE

            or

       uscan [--force]
EOF
}

[ "${1:-}" = "--upstream-version" ] \
    && [ -n "${2:-}" ] \
    && [ -n "${3:-}" ] \
    && [ -z "${4:-}" ] \
    || usage

TMPDIR=`mktemp -d -p .`

trap "rm -rf $TMPDIR" INT QUIT 0

VER="$2"
DEB_VER="${VER}.ds2"
UP_VER="${VER}"
UPSTREAM_TAR="$3"
UPSTREAM_DIR=Firebird-${UP_VER}
ORIG="../firebird2.1_${DEB_VER}.orig.tar.xz"
ORIG_DIR="firebird2.1-${DEB_VER}.orig"
TAR_COMPRESSION_FLAG=j
if echo $UPSTREAM_TAR | grep -E -q '\.xz$' ; then
    TAR_COMPRESSION_FLAG=J
fi

if [ -e "$ORIG" ]; then
    echo "$ORIG already exists. Aborting."
    exit 1
fi

echo -n "Expanding upstream source tree..."
if [ "$UPSTREAM_TAR" != "${UPSTREAM_TAR%.bz2}" ]; then
    tar xjf $UPSTREAM_TAR -C $TMPDIR
elif [ "$UPSTREAM_TAR" != "${UPSTREAM_TAR%.gz}" ]; then
    tar xzf $UPSTREAM_TAR -C $TMPDIR
elif [ "$UPSTREAM_TAR" != "${UPSTREAM_TAR%.xz}" ]; then
    tar xJf $UPSTREAM_TAR -C $TMPDIR
else
    echo "Don't know how to expand $UPSTREAM_TAR" >&2
    exit 1
fi
echo " done."

UPSTREAM_DIR=`ls -1 $TMPDIR`

# clean sources, needlessly supplied by upstream.
# Debian has packages for them already
# and generated files
echo -n "Cleaning upstream sources from unneeded things..."
for d in icu editline regex btyacc/test/ftp.y;
do
    echo -n " $d"
    rm -r $TMPDIR/$UPSTREAM_DIR/extern/$d
done
echo " done."

echo Removing files with no license...
cat debian/prune-upstream-dfsg.lst \
| while read f; do
    rm -rv $TMPDIR/$UPSTREAM_DIR/$f
done

echo -n "Removing generated files..."
for f in extern/btyacc/skeleton.c \
    src/include/gen/blrtable.h \
    configure \
    builds/make.new/config/config.h.in \
    builds/make.new/config/ltmain.sh \
    builds/make.new/config/config.guess;
do
    rm $TMPDIR/$UPSTREAM_DIR/$f || true
    echo " $f"
done
echo " done."

echo -n "Removing sourceless PDF files..."
find $TMPDIR/$UPSTREAM_DIR/doc -iname '*.pdf' -delete
echo " done."

echo -n "Cleaning other cruft..."
for f in .cvsignore;
do
    echo -n " $f"
    find . -name $f -delete
done
echo " done."

mv $TMPDIR/$UPSTREAM_DIR $TMPDIR/$ORIG_DIR

echo -n Repackaging into ${ORIG} ...
tar c -C $TMPDIR $ORIG_DIR | xz > "$ORIG"
echo " done."
