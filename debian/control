Source: firebird2.1
Section: database
Priority: optional
Maintainer: Debian Firebird Group <pkg-firebird-general@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>
Build-Depends: debhelper (>= 7), docbook-to-man, bison, dpkg-dev (>= 1.16.1~),
 libicu-dev, libedit-dev, libfbclient2 (>= 2.5.1.26349-0~rc1.ds4-1~),
 po-debconf,
 gawk, autoconf, libtool, automake, perl
Build-Conflicts: autoconf2.13, automake1.4
Standards-Version: 3.9.3
Vcs-Git: git://git.debian.org/git/pkg-firebird/2.1.git
Vcs-Browser: http://git.debian.org/?p=pkg-firebird/2.1.git;a=summary
Homepage: http://firebirdsql.org/

Package: firebird2.1-super
Architecture: any
Provides: firebird-server, firebird-utils
Conflicts: firebird-server, firebird2.0-classic, firebird2.0-super,
 firebird-utils
Depends: ${shlibs:Depends}, firebird2.1-common (= ${binary:Version}),
 firebird2.1-server-common (= ${binary:Version}),
 lsb-base, ${misc:Depends}, debconf (>= 1.4.69),
 firebird2.1-common-doc (= ${source:Version})
Recommends: libib-util
Suggests: firebird2.1-doc
Description: Firebird Super Server - an RDBMS based on InterBase 6.0 code
 Firebird is a relational database offering many ANSI SQL-99 features that
 runs on Linux, Windows, and a variety of Unix platforms. Firebird offers
 excellent concurrency, high performance, and powerful language support
 for stored procedures and triggers. It has been used on production systems
 under a variety of names since 1981.
 .
 The "super" architecture uses separate threads to handle each connection.
 It has its advantages (eg: is usually faster and more efficient for large
 numbers of clients) but is unable to use more that one CPU on an SMP
 system and under some circumstances a client may crash all server threads.
 .
 This package contains the 2.1 branch of Firebird.
 .
 Firebird is a commercially independent project of C and C++ programmers,
 technical advisors and supporters developing and enhancing a multi-platform
 relational database management system based on the source code released by
 Inprise Corp (now known as Borland Software Corp) under the InterBase Public
 License v.1.0 on 25 July, 2000.

Package: firebird2.1-classic
Architecture: any
Provides: firebird-server, firebird-utils
Conflicts: firebird-server, firebird2.0-super, firebird2.0-classic,
 firebird-utils
Depends: ${shlibs:Depends}, firebird2.1-common (= ${binary:Version}), netbase,
 firebird2.1-server-common (= ${binary:Version}),
 openbsd-inetd | inet-superserver, ${misc:Depends},
 debconf (>= 1.4.69),
 firebird2.1-common-doc (= ${source:Version})
Recommends: libib-util
Suggests: firebird2.1-doc
Description: Firebird Classic Server - an RDBMS based on InterBase 6.0 code
 Firebird is a relational database offering many ANSI SQL-99 features that
 runs on Linux, Windows, and a variety of Unix platforms. Firebird offers
 excellent concurrency, high performance, and powerful language support
 for stored procedures and triggers. It has been used on production systems
 under a variety of names since 1981.
 .
 The "classic" architecture uses a new process to handle each connection
 which results in somewhat slower operation (but is said to be faster
 than "super" on local connections) yet can take advantage of multiple
 CPUs on SMP machines. This is the "traditional" architecture.
 .
 This package contains the 2.1 branch of Firebird.
 .
 Firebird is a commercially independent project of C and C++ programmers,
 technical advisors and supporters developing and enhancing a multi-platform
 relational database management system based on the source code released by
 Inprise Corp (now known as Borland Software Corp) under the InterBase Public
 License v.1.0 on 25 July, 2000.

Package: libfbembed2.1
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, firebird2.1-common (= ${binary:Version}),
 firebird2.1-server-common (= ${binary:Version}), ${misc:Depends},
 firebird2.1-common-doc (= ${source:Version})
Description: Firebird embedded client/server library
 Firebird is a relational database offering many ANSI SQL-99 features that
 runs on Linux, Windows, and a variety of Unix platforms. Firebird offers
 excellent concurrency, high performance, and powerful language support
 for stored procedures and triggers. It has been used on production systems
 under a variety of names since 1981.
 .
 This package contains libfbembed - the embedded client/server library for
 Firebird.
 .
 It can work with remote Firebird servers (either super-server or classic) via
 TCP connections, in the same way libfbclient does.
 .
 Contrary to libfbclient, libfbembed is not thread-safe.
 .
 When working with local database libfbembed works directly with the database
 file without the need of a separate server process. It needs a separate lock
 manager -- fb_lock_mgr -- which is in firebird2.1-classic package.
 .
 If you are in doubt, you most probably need libfbclient2, instead of this
 package.

Package: firebird2.1-common
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
 firebird2.1-common-doc (= ${source:Version})
Description: common files for firebird 2.1 servers and clients
 Firebird is a relational database offering many ANSI SQL-99 features that
 runs on Linux, Windows, and a variety of Unix platforms. Firebird offers
 excellent concurrency, high performance, and powerful language support
 for stored procedures and triggers. It has been used on production systems
 under a variety of names since 1981.
 .
 This package contains files common to both servers and clients for both super
 and classic flavours.
 .
 These include the message file (firebird.msg), support scripts as well as
 configuration files in /etc/firebird/2.1.
 .
 Please refer to the firebird2.1-super or firebird2.1-classic packages for more
 information about Firebird in general.

Package: firebird2.1-server-common
Architecture: any
Depends: adduser, ${misc:Depends},
 firebird2.1-common-doc (= ${source:Version})
Conflicts: firebird2.0-server-common
Description: common files for firebird 2.1 servers
 This package contains files common to super and classic Firebird servers.
 .
 These include the security database, manual pages, support scripts as well as
 configuration files in /etc/firebird/2.1.
 .
 Please refer to the firebird2.1-super or firebird2.1-classic packages for more
 information about Firebird in general.

Package: firebird2.1-examples
Section: doc
Architecture: all
Depends: ${misc:Depends},
 firebird2.1-common-doc (= ${source:Version})
Recommends: firebird-dev
Description: Examples for Firebird - an RDBMS based on InterBase 6.0 code
 The examples included in this package are provided as samples and are also
 used by Firebird's testing suite (TCS). The examples are in the form
 of .h, .c, .e, .fdb and .gbk files.
 .
 Please refer to the firebird2.1-super and firebird2.1-classic packages for
 more information about Firebird in general.

Package: firebird2.1-doc
Section: doc
Architecture: all
Depends: ${misc:Depends},
 firebird2.1-common-doc (= ${source:Version})
Description: Documentation files for firebird database version 2.1
 Various documents for firebird 2.1. These include the release notes, what's
 new in firebird 2.1, quick start guide, upgrade guide, list of ISQL
 enhancements etc.

Package: firebird2.1-common-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: copyright, licensing and changelogs of firebird2.1
 This is an (otherwise) empty package contains the copyright and licensing
 information of firebird2.1, as well as the upstream changelog.
 .
 Due to the size of the upstream change log and copyright/licensing summary,
 keeping them in a separate package saves space on mirrors and when installing
 firebird2.1 packages.
 .
 Please refer to the firebird2.1-super or firebird2.1-classic packages for more
 information about Firebird database in general.
