# The contents of this file are subject to the Interbase Public
# License Version 1.0 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy
# of the License at http://www.Inprise.com/IPL.html
#
# Software distributed under the License is distributed on an
# "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express
# or implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code was created by Inprise Corporation
# and its predecessors. Portions created by Inprise Corporation are
#
# Copyright (C) 2000 Inprise Corporation
# All Rights Reserved.
# Contributor(s): ______________________________________.
# Start of file prefix.darwin:	$(VERSION)	@PLATFORM@
#$Id: prefix.darwin_ppc64,v 1.1.2.3 2008-06-09 14:01:05 paulbeach Exp $
# 2 Oct 2002, Nickolay Samofatov - Major Cleanup

# To use this file to build 64bit version of Firebird for MacOSX 10.5 (Leopard)
# Can only be built on MacOSX 10.5 (Leopard) due to lack of 64bit support in 
# Carbon in previous versions of MacOSX
# 1. edit configure.in so that MAKEFILE_PREFIX=darwin_ppc64
# 2. uncomment the CPU_TYPE
# 3. edit extern/icu/source/config/mh-darwin and set the right 64bit flags (-arch ppc64)
# for CFLAGS, CXXFLAGS & LD_FLAGS
# 4. export CFLAGS='-arch _ppc64'
# 5. export CXXFLAGS='-arch ppc_64'
# 6. export LDFLAGS='-arch ppc_64'
# 7. export MACOSX_DEPLOYMENT_TARGET=10.5

DYLD_LIBRARY_PATH=$(FIREBIRD)/lib
export DYLD_LIBRARY_PATH

MACOSX_DEPLOYMENT_TARGET=10.5
export MACOSX_DEPLOYMENT_TARGET

OS_ServerFiles=inet_server.cpp

PROD_FLAGS=-O3 -DNDEBUG -DDARWIN -pipe -p -MMD -fPIC -fno-common -mmacosx-version-min=10.5
DEV_FLAGS=-ggdb -DDARWIN -pipe -p -MMD -fPIC -fno-common -Wall -mmacosx-version-min=10.5
CXXFLAGS:=$(CXXFLAGS) -fvisibility-inlines-hidden -fvisibility=hidden
EMBED_UTIL_TARGETS=gstat gds_drop gds_relay gsec nbackup fb_lock_print fbsvcmgr
CLIENT_UTIL_TARGETS=gds_drop gds_relay gstat gsec fbguard fbmgr_bin nbackup fb_lock_print fbsvcmgr

Physical_IO_Module=os/posix/unix.cpp
PLATFORM_PATH=os/darwin

LINK_OPTS:=-arch ppc64 
LD_FLAGS+=-arch ppc64
LIB_BUNDLE_OPTIONS:=$(LD_FLAGS) -bundle -flat_namespace -undefined suppress
LIB_LINK_OPTIONS:=$(LD_FLAGS) -dynamiclib -flat_namespace
LIB_LINK_SONAME:=-current_version @FIREBIRD_VERSION@ -compatibility_version @FIREBIRD_VERSION@ -seg1addr 0x30000000
LIB_LINK_MAPFILE:=-Wl,-exported_symbols_list,
LINK_FIREBIRD_EMBED_SYMBOLS=$(LIB_LINK_MAPFILE)$(ROOT)/builds/posix/firebird.darwin.embed.vers
LINK_FIREBIRD_CLIENT_SYMBOLS=$(LIB_LINK_MAPFILE)$(ROOT)/builds/posix/firebird.darwin.client.vers
LINK_FBINTL_SYMBOLS=$(LIB_LINK_MAPFILE)$(ROOT)/builds/posix/fbintl.darwin.vers

LIB_LINK_RPATH:=
LIB_LINK_DYNAMIC:=-install_name /Library/Frameworks/Firebird.framework/Versions/A/Libraries/
LIB_EMBED_LINK_OPTIONS:=-install_name /Library/Frameworks/Firebird.framework/Versions/A/Firebird
LIB_CLIENT_LINK_OPTIONS:=-install_name /Library/Frameworks/Firebird.framework/Versions/A/Firebird
FBEMBED_LINK:=-F../gen/firebird -framework Firebird -L$(LIB) -lfbembed
PLATFORM_FALLBACK=os/posix
PLAT_CLASSIC_PRE_TARGET=darwin_setup_framework
PLAT_CLASSIC_POST_TARGET=darwin_finish_cs_framework

PLAT_SUPER_PRE_TARGET=darwin_setup_framework
PLAT_SUPER_POST_TRAGET=darwin_finish_ss_framework

PLATFORM_POSTBUILD_TARGET=darwin_postbuild_target
